package uz.pdp.db;

import lombok.SneakyThrows;
import uz.pdp.model.Book;
import uz.pdp.model.Customer;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DbService {
    public static final String DB_URL = "jdbc:postgresql://localhost:5432/exam_spring_module1";
    public static final String DB_USER = "postgres";
    public static final String DB_PASS = "root123";

    @SneakyThrows
    public static Connection getConnection() {
        Class.forName("org.postgresql.Driver");
        return DriverManager.getConnection(DB_URL, DB_USER, DB_PASS);
    }

    public static void addStudent(Customer customer) throws SQLException {
        Connection connection = getConnection();
        Statement statement = connection.createStatement();
        String query = "insert into customer(username,password) values  ('"
                + customer.getUsername() + "'," + customer.getPassword() + ")";
        statement.executeUpdate(query);
    }

    public static boolean checkUser(Customer customer) throws SQLException {
        Connection connection = getConnection();
        String query = "select * from customer where username = ? and password = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, customer.getUsername());
        preparedStatement.setString(2, customer.getPassword());
        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            return true;
        }
        return false;
    }


    public static void updatedBook(Book book) throws SQLException {
        Connection connection = getConnection();
        Statement statement = connection.createStatement();
        String query = "update book set book_name='" +
                book.getBook_name() + "', author='" +
                book.getAuthor() + "', price='" + book.getPrice() + "' where id=" + book.getId() + ";";
        statement.execute(query);
    }

    @SneakyThrows
    public static Book getBookByIdFromDb(Integer id) throws SQLException {
        Connection connection = getConnection();
        Statement statement = connection.createStatement();
        String query = "select * from book where id=" + id;
        ResultSet resultSet = statement.executeQuery(query);
        Book book = new Book();
        while (resultSet.next()) {
            book.setId(resultSet.getInt(1));
            book.setBook_name(resultSet.getString(2));
            book.setAuthor(resultSet.getString(3));
            book.setPrice(resultSet.getDouble(4));
        }
        return book;
    }

    public static List<Book> getAllBooks() throws SQLException {
        List<Book> bookList = new ArrayList<>();
        Connection connection = getConnection();
        Statement statement = connection.createStatement();
        String query = "select * from book";
        ResultSet resultSet = statement.executeQuery(query);
        while (resultSet.next()) {
            bookList.add(new Book(
                    resultSet.getInt(1),
                    resultSet.getString(2),
                    resultSet.getString(3),
                    resultSet.getDouble(4)));
        }
        return bookList;
    }

    public static void addBook(Book book) throws SQLException {
        Connection connection = getConnection();
        String query = "insert into book (book_name, author, price) values (?, ?, ?)";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, book.getBook_name());
        preparedStatement.setString(2, book.getAuthor());
        preparedStatement.setDouble(3, book.getPrice());
        preparedStatement.executeUpdate();
    }


    @SneakyThrows
    public static void deleteBookByIdFromDb(Integer id) throws SQLException {
        Connection connection = getConnection();
        Statement statement = connection.createStatement();
        String query = "delete from book where id=" + id;
        statement.execute(query);
    }



}