package uz.pdp.controller;

import lombok.SneakyThrows;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import uz.pdp.db.DbService;
import uz.pdp.model.Book;
import uz.pdp.model.Customer;

import java.sql.SQLException;
import java.util.List;

@Controller
public class CustomerController {

    @GetMapping("/singUp")
    public String singUpForm() {
        return "signIn";
    }


    @GetMapping("/loginUser")
    public String login() {
        return "loginForm";
    }


    @PostMapping("/loginByUser")
    public String checkUser(@RequestParam String username, @RequestParam String password) throws SQLException {
        boolean checkUser = DbService.checkUser(new Customer(username, password));
        if (checkUser) {
            return "book_main_page";
        }
        return "index";
    }


    @SneakyThrows
    @PostMapping("/login")
    public String save(@RequestParam String username, @RequestParam String password) {
        DbService.addStudent(new Customer(username, password));
        return "book_main_page";
    }


    @GetMapping("/book/all")
    public String getAllBooks(Model model) throws SQLException {
        List<Book> bookList = DbService.getAllBooks();
        model.addAttribute("all_Books", bookList);
        return "allBooks";
    }


    @GetMapping("/book_main_page")
    public String redirectToBookPage() {
        return "book_main_page";
    }


    @GetMapping("/book/save")
    public String createBookForm() {
        return "createSaveForm";
    }


    @PostMapping("/book/save")
    public String saveTheBook(@RequestParam String bookName, @RequestParam String author, @RequestParam Double price, Model model) throws SQLException {
        DbService.addBook(new Book(bookName, author, price));
        List<Book> allBooks = DbService.getAllBooks();
        model.addAttribute("all_Books", allBooks);
        return "allBooks";
    }


    @GetMapping("/book/edit")
    public String editBook(@RequestParam("id") Integer id, Model model) throws SQLException {
        Book bookByIdFromDb = DbService.getBookByIdFromDb(id);
        model.addAttribute("updateBook", bookByIdFromDb);
        return "update";
    }


    @PostMapping("/book/edit")
    public String updatedStudent(@RequestParam Integer id,
                                 @RequestParam String book_name,
                                 @RequestParam String author,
                                 @RequestParam Double price,
                                 Model model) throws SQLException {
        DbService.updatedBook(new Book(id, book_name, author, price));
        List<Book> allBooks = DbService.getAllBooks();
        model.addAttribute("all_Books", allBooks);
        return "allBooks";
    }


    @GetMapping("/book/delete")
    public String deleteBookByIdFromDb(@RequestParam("id") Integer id, Model model) throws SQLException {
        DbService.deleteBookByIdFromDb(id);
        List<Book> allBooks = DbService.getAllBooks();
        model.addAttribute("all_Books", allBooks);
        return "allBooks";
    }

}
