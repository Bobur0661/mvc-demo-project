package uz.pdp.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Book {

    private Integer id;
    private String book_name;
    private String author;
    private double price;

    public Book(String book_name, String author, double price) {
        this.book_name = book_name;
        this.author = author;
        this.price = price;
    }
}
